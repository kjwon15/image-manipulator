from PIL import ImageFilter


FILTERS = {}


def image_filter(filtername):
    def decorator(func):
        FILTERS[filtername] = func
        return func
    return decorator


def apply_filter(image, mask, builtin=False):
    if builtin:
        return image.filter(ImageFilter.Kernel((3, 3), mask))

    weight = sum(mask)

    has_alpha = image.mode == 'RGBA'
    is_mono = image.mode == 'L'

    if has_alpha:
        alpha = image.split()[-1]

    if not is_mono:
        image = image.convert('HSV')
    new_image = image.copy()

    pixmap = image.load()
    new_pixmap = new_image.load()

    for px in range(1, image.size[0]-1):
        for py in range(1, image.size[1]-1):
            value = 0

            for mask_index in range(9):
                x = px + (mask_index // 3) - 1
                y = py + (mask_index % 3) - 1

                if is_mono:
                    value += mask[mask_index] * pixmap[x, y]
                else:
                    value += mask[mask_index] * pixmap[x, y][2]

            pixel = pixmap[px, py]
            if is_mono:
                if weight:
                    new_pixmap[px, py] = value // weight
                else:
                    new_pixmap[px, py] = value + 127
            else:
                if weight:
                    new_pixmap[px, py] = (pixel[0], pixel[1], value // weight)
                else:
                    new_pixmap[px, py] = (pixel[0], pixel[1], value + 127)

    new_image = new_image.convert('RGBA')

    if has_alpha:
        new_image.putalpha(alpha)

    return new_image


@image_filter('averaging')
def averaging(image, builtin=False):
    return apply_filter(
        image,
        [1 for _ in range(9)],
        builtin=builtin)


@image_filter('median')
def median(image, builtin=False):
    if builtin:
        return image.filter(ImageFilter.MedianFilter(3))

    def get_lightness(pixel):
        return pixel[0] * .299 + pixel[1] + .587 + pixel[2] + .114

    def med(lst):
        lst = sorted(lst, key=get_lightness)
        return lst[len(lst)/2]

    pixels = image.load()
    original = image.copy().load()

    for x in range(image.size[0]):
        for y in range(image.size[1]):
            pixels[x, y] = med(
                original[i, j]
                for i in range(max(0, x-1),
                               min(x+2, image.size[0]))
                for j in range(max(0, y-1),
                               min(y+2, image.size[1]))
            )

    return image


@image_filter('edge enhance')
def edge_enhance(image, builtin=False):
    if builtin:
        return image.filter(ImageFilter.EDGE_ENHANCE)

    return apply_filter(
        image,
        [0, -1, 0,
         -1, 5, -1,
         0, -1, 0])


@image_filter('edge enhance more')
def edge_enhance_more(image, builtin=False):
    if builtin:
        return image.filter(ImageFilter.EDGE_ENHANCE_MORE)

    return apply_filter(
        image,
        [-1, -1, -1,
         -1, 9, -1,
         -1, -1, -1])


@image_filter('sharpening')
def sharpening(image, builtin=False):

    if builtin:
        return image.filter(ImageFilter.SHARPEN)

    has_alpha = image.mode == 'RGBA'

    if has_alpha:
        alpha = image.split()[-1]

    blurred = image.filter(ImageFilter.BLUR).convert('HSV')
    mask = image.copy().convert('HSV')
    new_image = image.copy().convert('HSV')

    blurred_pixmap = blurred.load()
    mask_pixmap = mask.load()
    new_pixmap = new_image.load()

    for x in range(image.size[0]):
        for y in range(image.size[1]):
            h = mask_pixmap[x, y][0]
            s = mask_pixmap[x, y][1]
            v = mask_pixmap[x, y][2] - blurred_pixmap[x, y][2]

            mask_pixmap[x, y] = (h, s, v)

    for x in range(image.size[0]):
        for y in range(image.size[1]):
            h = new_pixmap[x, y][0]
            s = new_pixmap[x, y][1]
            v = new_pixmap[x, y][2] + mask_pixmap[x, y][2]

            new_pixmap[x, y] = (h, s, v)

    new_image = new_image.convert('RGB')

    if has_alpha:
        new_image.putalpha(alpha)

    return new_image


@image_filter('emboss')
def emboss(image, builtin=False):
    if builtin:
        return image.filter(ImageFilter.EMBOSS)

    return apply_filter(
        image,
        [-1, -1, 0,
         -1, 0, 1,
         0, 1, 1])


@image_filter('laplacian')
def laplacian(image, builtin=False):

    return apply_filter(
        image.convert('L'),
        [0, -1, 0,
         -1, 4, -1,
         0, -1, 0])


@image_filter('laplacian2')
def laplacian2(image, builtin=False):

    return apply_filter(
        image.convert('L'),
        [-1, -1, -1,
         -1, 8, -1,
         -1, -1, -1])


@image_filter('sobel')
def sobel(image, builtin=False):
    image = image.convert('L')

    img1 = apply_filter(
        image,
        [-1, -2, -1,
         0, 0, 0,
         1, 2, 1])

    img2 = apply_filter(
        image,
        [-1, 0, 1,
         -2, 0, 2,
         -1, 0, 1])

    new_image = image.copy()
    pixmap = new_image.load()
    pix1 = img1.load()
    pix2 = img2.load()

    for x in range(new_image.size[0]):
        for y in range(new_image.size[1]):
            l = int((pix1[x, y][2]**2 + pix2[x, y][2]**2) ** .5)
            pixmap[x, y] = l

    return new_image


@image_filter('prewitt')
def prewitt(image, builtin=False):
    return apply_filter(
        image,
        [-1, 1, 1,
         -1, -2, 1,
         -1, 1, 1])


@image_filter('motion blur')
def motion_blur(image, builtin=False):
    return apply_filter(
        image,
        [1, 0, 0,
         0, 1, 0,
         0, 0, 1])


@image_filter('excessive')
def excessive(image, builtin=False):
    return apply_filter(
        image,
        [1, 1, 1,
         1, -7, 1,
         1, 1, 1])


@image_filter('gaussian')
def gaussian(image, builtin=False):
    return apply_filter(
        image,
        [1, 2, 1,
         2, 4, 2,
         1, 2, 1])
