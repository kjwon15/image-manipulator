function dropHandler(event) {
  event.preventDefault();
  event.stopPropagation();

  event = event.originalEvent;

  loadImage(event.dataTransfer.files[0] || event.dataTransfer.getData('url'))
}

function enterHandler(event) {
  event.preventDefault();
  event.stopPropagation();
}

function leaveHandler(event) {
  event.preventDefault();
  event.stopPropagation();
}

function overHandler(event) {
  event.preventDefault();
  event.stopPropagation();
  event.originalEvent.dataTransfer.dropEffect = 'copy';
}

function changeHandler(event) {
  var file = event.target.files[0];
  loadImage(file);
}

function loadImage(file) {
  var imgInput = $('#img-preview');

  // If image from google image search
  if (/https?:\/\/(www\.)?google\.com\/imgres/.test(file)) {
    var url = /imgurl=(.*?)&/.exec(file)[1];
    return loadImage(url);
  }

  if (imgInput.length == 0) {
    imgInput = $('<img>').attr('id', 'img-preview');
    $('#drag-box').append(imgInput);
  }

  if (file instanceof File) {
    if (file.type.startsWith('image/') === false) {
      alert('This is not an image file!');
      return;
    }

    var fileReader = new FileReader();
    fileReader.onloadend = function() {
      imgInput.attr('src', fileReader.result);
    };

    fileReader.readAsDataURL(file);
  } else if (file.startsWith('data:')) {
    imgInput.attr('src', file);
  } else {
    imgInput.attr('src', GET_IMG_URL + encodeURIComponent(file));
  }
}

function getImage() {
  var img = $('#img-preview').get(0);
  var canvas = document.createElement('canvas');
  var context = canvas.getContext('2d');

  canvas.width = img.naturalWidth;
  canvas.height = img.naturalHeight;
  context.drawImage(img, 0, 0);

  return canvas.toDataURL();
}

function formHandler(event) {
  event.preventDefault();

  $('#output-box').addClass('loading');

  $.post($(this).attr('action'),
      {
        filtername: $('#filter-name').val(),
        filtervalue: $('#filter-value').val(),
        builtin: $('#builtin').is(':checked'),
        image: getImage(),
      }
  ).success(function(data) {
    var output = $('#img-output');
    if (output.length === 0) {
      output = $('<img>');
      output.attr('id', 'img-output');
      $('#output-box').append(output);
    }

    output.attr('src', data);
  }).always(function() {
    $('#output-box').removeClass('loading');
  });
}

$(function() {
  $('#drag-box').on('dragenter', enterHandler);
  $('#drag-box').on('dragleave', leaveHandler);
  $('#drag-box').on('dragover', overHandler);
  $('#drag-box').on('drop', dropHandler);

  $('#input-file').on('change', changeHandler);
  $('#main-form').submit(formHandler);
});
