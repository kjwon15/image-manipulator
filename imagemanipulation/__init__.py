import base64
import requests
from PIL import Image
from io import BytesIO
from flask import Flask, Response, render_template, request

from .filters import FILTERS


app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/get_image/', defaults={'url': None})
@app.route('/get_image/<path:url>', methods=['GET'])
def get_image(url):
    response = requests.get(url)
    image = response.content
    return Response(image, mimetype=response.headers['content-type'])


@app.route('/do_filter/', methods=['POST'])
def do_filter():
    filter_name = request.form['filtername']
    use_builtin = True if request.form['builtin'].lower() == 'true' else False

    imageData = base64.decodestring(
        request.form['image'].partition('base64,')[2].encode('ascii'))
    image = Image.open(BytesIO(imageData))

    image = FILTERS[filter_name](image, builtin=use_builtin)

    output = BytesIO()
    image.save(output, format='PNG')

    return Response('data:image/png;base64,' +
                    base64.encodebytes(output.getvalue()).decode('ascii'))


def get_filters():
    return sorted(FILTERS.keys())


@app.context_processor
def utility_processor():
    return {
        'get_filters': get_filters
    }


if __name__ == '__main__':
    app.run(debug=True)
